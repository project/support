**About This Branch**
This is a complete rewrite and rearchitecture of the Drupal support module
(http://drupal.org/project/support).

**Current status**
* As of October 9 2023, we're upgrading architecture from Drupal Eight to Drupal Ten.

**Future suggested architecture**
* Clients: enforced using Organic Groups
* Tickets: custom entity type, supporting optional time tracking and billing.
* Ticket listings: built with Views
* Mail integration: plan to implement using OG Mailinglist

Support-specific entities and functionality will live in the Support project.
Additional configuration and dependencies will be configured in an install
profile.

There will be no migration path from Drupal Seven and Eight to the new architecture.

**Sponsor**
Development on the Support module is originally sponsored by Tag1 Consulting
and now is maintained by Purencool Digital. Currently, the 2.x branch is being
upgraded for a future release in Drupal Ten or Eleven.

**Development**
Commmands used to assist in testing the modules code integrity inline with
[Drupal](https://www.drupal.org/docs/develop/standards) development standards.

Run the following command from the support module directory to test to the
codebase is complaint using [phpcs](https://www.drupal.org/docs/contributed-modules/code-review-module/php-codesniffer-command-line-usage).
```
phpcs \
--standard=Drupal,DrupalPractice \
--extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml  *
```
