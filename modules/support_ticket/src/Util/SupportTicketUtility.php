<?php

namespace Drupal\support_ticket\Util;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\support_ticket\SupportTicketTypeInterface;

/**
 * Makes Support Add Body Fields.
 */
class SupportTicketUtility {

  /**
   * Adds the default body field to a support ticket type.
   *
   * @param \Drupal\support_ticket\SupportTicketTypeInterface $type
   *   A support ticket type object.
   * @param string $label
   *   (optional) The label for the body instance.
   *
   * @return \Drupal\field\FieldConfigInterface
   *   Ticket body field.
   */
  public static function supportTicketAddBodyField(SupportTicketTypeInterface $type, string $label = 'Ticket body') {

    $field = FieldConfig::loadByName('support_ticket', $type->id(), 'body');
    if (empty($field)) {
      \Drupal::entityTypeManager()
        ->getStorage('field_config')
        ->create([
          'field_storage' => FieldStorageConfig::loadByName('support_ticket', 'body'),
          'bundle' => $type->id(),
          'label' => $label,
          'settings' => ['display_summary' => TRUE],
        ])
        ->save();

      // @todo when creating a new support ticket type this sets the body field widget to visible.
      // is it needed at all.
      // Assign widget settings for the 'default' form mode.
      // \Drupal::service('support_ticket')
      // ->getFormDisplay( $type->id(), 'default')
      // ->save();
      // Assign display settings for the 'default' and 'teaser' view modes.
      // \Drupal::service('support_ticket')
      // ->getViewDisplay($type->id(), 'default')
      // ->save();
    }

    return $field;
  }

}
