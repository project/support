<?php

namespace Drupal\support_ticket\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller renders a single html support ticket.
 *
 * The SupportTicketViewController renders a html support ticket. The class
 * is initialized using the "/support_ticket/{ticket number}" route pattern.
 */
class SupportTicketViewController extends EntityViewController {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Creates a NodeViewController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    parent::__construct($entity_type_manager, $renderer);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('entity_type.manager'), $container
      ->get('renderer'), $container
      ->get('entity.repository'));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function view(EntityInterface $support_ticket, $view_mode = 'full', $language_code = NULL): array {

    // Creates view from EntityViewController and returns an array.
    $build = parent::view($support_ticket);

    // Adds the \Drupal\support_ticket\Entity\SupportTicket entity URL
    // relationships from generated configuration to the html document header.
    foreach ($support_ticket->uriRelationships() as $rel) {

      // Sets the support ticket path as the canonical URL to
      // prevent duplicate tickets.
      $build['#attached']['html_head_link'][] = [
        [
          'rel' => $rel,
          'href' => $support_ticket->toUrl($rel)->toString(),
        ],
        TRUE,
      ];

      // Sets the non-aliased canonical path as a default shortlink.
      if ($rel == 'canonical') {
        $build['#attached']['html_head_link'][] = [
          [
            'rel' => 'shortlink',
            'href' => $support_ticket->toUrl($rel, ['alias' => TRUE])->toString(),
          ],
          TRUE,
        ];
      }
    }

    return $build;
  }

  /**
   * The _title_callback for the page that renders a single support ticket.
   *
   * Allows the page title(label) to be translated if needed using the
   * EntityRepositoryInterface service that has been added by dependency
   * injection to the ContainerInterface.
   *
   * @param \Drupal\Core\Entity\EntityInterface $support_ticket
   *   The current support ticket.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $support_ticket): string {
    return $this->entityRepository->getTranslationFromContext($support_ticket)->label();
  }

}
