<?php

namespace Drupal\support_ticket\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Controller\EntityViewController;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a controller to render a single support ticket in preview.
 *
 * The SupportTicketPreviewController renders a html support ticket. The class
 * is initialized using the "/support_ticket/preview/{param}/{param}"
 * route pattern.
 */
class SupportTicketPreviewController extends EntityViewController {

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * Creates a NodeViewController object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer, EntityRepositoryInterface $entity_repository) {
    parent::__construct($entity_type_manager, $renderer);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container
      ->get('entity_type.manager'), $container
      ->get('renderer'), $container
      ->get('entity.repository'));
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function view(EntityInterface $support_ticket_preview, $view_mode_id = 'full', $language_code = NULL): array {
    $support_ticket_preview->preview_view_mode = $view_mode_id;
    $build = parent::view($support_ticket_preview, $view_mode_id);

    $build['#attached']['library'][] = 'support_ticket/drupal.support_ticket.preview';

    // Don't render cache previews.
    unset($build['#cache']);

    foreach ($support_ticket_preview->uriRelationships() as $rel) {
      // Set the support ticket path as the canonical URL to prevent
      // duplicate tickets.
      $build['#attached']['html_head_link'][] = [
        [
          'rel' => $rel,
          'href' => $support_ticket_preview->toUrl($rel)->toString(),
        ],
        TRUE,
      ];

      if ($rel == 'canonical') {
        // Set the non-aliased canonical path as a default shortlink.
        $build['#attached']['html_head_link'][] = [
          [
            'rel' => 'shortlink',
            'href' => $support_ticket_preview->toUrl($rel, ['alias' => TRUE])->toString(),
          ],
          TRUE,
        ];
      }
    }

    return $build;
  }

  /**
   * The $support_ticket_preview for the page.
   *
   * Allows the page title(label) to be translated if needed using the
   * EntityRepositoryInterface service that has been added by dependency
   * injection to the ContainerInterface.
   *
   * @param \Drupal\Core\Entity\EntityInterface $support_ticket_preview
   *   The current support ticket.
   *
   * @return string
   *   The page title.
   */
  public function title(EntityInterface $support_ticket_preview): string {
    return $this->entityRepository->getTranslationFromContext($support_ticket_preview)->label();
  }

}
