<?php

namespace Drupal\support_ticket\Plugin\views\argument;

use Drupal\user\Plugin\views\argument\Uid;

/**
 * Checks for users revisions.
 *
 * Filter handler to accept a user id to check for support tickets that
 * user posted or created a revision on.
 *
 * @ViewsArgument("support_ticket_uid_revision")
 */
class UidRevision extends Uid {

  /**
   * Uid Revision.
   *
   * @param mixed $group_by
   *   Sort by group type.
   */
  public function query($group_by = FALSE) {
    $this->ensureMyTable();
    $placeholder = $this->placeholder();
    $this->query->addWhereExpression(
       0,
       "$this->tableAlias.revision_uid = $placeholder OR 
       ((SELECT COUNT(DISTINCT vid) FROM 
       {support_ticket_revision} nr WHERE 
       nfr.revision_uid = $placeholder AND 
       nr.stid = $this->tableAlias.stid) > 0)",
       [$placeholder => $this->argument]
     );
  }

}
