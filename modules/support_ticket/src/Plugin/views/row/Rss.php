<?php

namespace Drupal\support_ticket\Plugin\views\row;

use Drupal\Core\Render\Markup;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\views\Plugin\views\row\RssPluginBase;

/**
 * Performs a support_ticket_view on the resulting object.
 *
 * Plugin which performs a support_ticket_view on the resulting object
 * and formats it as an RSS item.
 *
 * @ViewsRow(
 *   id = "support_ticket_rss",
 *   title = @Translation("Tickets"),
 *   help = @Translation("Display the tickets with standard support ticket view."),
 *   theme = "views_view_row_rss",
 *   register_theme = FALSE,
 *   base = {"support_ticket_field_data"},
 *   display_types = {"feed"}
 * )
 */
class Rss extends RssPluginBase {

  /**
   * Basic properties that let the row style follow relationships.
   *
   * @var string
   */
  protected $baseTable = 'support_ticket_field_data';

  /**
   * Basic properties that let the row style follow relationships.
   *
   * @var string
   */
  protected $baseField = 'stid';

  /**
   * Stores the support tickets loaded with preRender.
   *
   * @var string
   */
  protected $supportTickets = [];

  /**
   * {@inheritdoc}
   */
  protected $entityTypeId = 'support_ticket';

  /**
   * The support ticket storage.
   *
   * @var \Drupal\support_ticket\SupportTicketStorageInterface
   */
  protected $supportTicketStorage;

  /**
   * Constructs the Rss object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_manager);
    $this->supportTicketStorage = $entity_manager->getStorage('support_ticket');
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm_summary_options() {
    $options = parent::buildOptionsForm_summary_options();
    $options['title'] = $this->t('Title only');
    $options['default'] = $this->t('Use site default RSS settings');
    return $options;
  }

  /**
   * Summary Title string.
   *
   * @return string
   *   Returning title string.
   */
  public function summaryTitle() {
    $options = $this->buildOptionsForm_summary_options();
    return Markup::create($options[$this->options['view_mode']]);
  }

  /**
   * Prerender method.
   *
   * @param array $values
   *   Array of values to be render.
   */
  public function preRender($values) {
    $stids = [];
    foreach ($values as $row) {
      $stids[] = $row->{$this->field_alias};
    }
    if (!empty($stids)) {
      $this->support_tickets = $this->supportTicketStorage->loadMultiple($stids);
    }
  }

  /**
   * Render method.
   *
   * @param object $row
   *   Object of rows.
   *
   * @global string $base_url
   *   System URL.
   *
   * @return array
   *   Array to be rendered.
   */
  public function render($row) {
    global $base_url;

    $stid = $row->{$this->field_alias};
    if (!is_numeric($stid)) {
      return [];
    }

    $display_mode = $this->options['view_mode'];
    if ($display_mode == 'default') {
      $display_mode = \Drupal::config('system.rss')->get('items.view_mode');
    }

    // Load the specified support ticket:
    /** @var \Drupal\support_ticket\SupportTicketInterface $support_ticket */
    $support_ticket = $this->support_tickets[$stid];
    if (empty($support_ticket)) {
      return [];
    }

    $description_build = [];

    $support_ticket->link = '';
    $support_ticket->url('canonical', ['absolute' => TRUE]);
    $support_ticket->rss_namespaces = [];
    $support_ticket->rss_elements = [
      [
        'key' => 'pubDate',
        'value' => gmdate('r', $support_ticket->getCreatedTime()),
      ],
      [
        'key' => 'dc:creator',
        'value' => $support_ticket->getOwner()->getDisplayName(),
      ],
      [
        'key' => 'guid',
        'value' => $support_ticket->id() . ' at ' . $base_url,
        'attributes' => ['isPermaLink' => 'false'],
      ],
    ];

    // The support ticket gets built and modules add to or
    // modify $support_ticket->rss_elements
    // and $support_ticket->rss_namespaces.
    $build_mode = $display_mode;

    $build = support_ticket_view($support_ticket, $build_mode);
    unset($build['#theme']);

    if (!empty($support_ticket->rss_namespaces)) {
      $this->view->style_plugin->namespaces = array_merge($this->view->style_plugin->namespaces, $support_ticket->rss_namespaces);
    }
    elseif (function_exists('rdf_get_namespaces')) {
      // Merge RDF namespaces in the XML namespaces in case they are used
      // further in the RSS content.
      $xml_rdf_namespaces = [];
      foreach (rdf_get_namespaces() as $prefix => $uri) {
        $xml_rdf_namespaces['xmlns:' . $prefix] = $uri;
      }
      $this->view->style_plugin->namespaces += $xml_rdf_namespaces;
    }

    if ($display_mode != 'title') {
      // We render support ticket contents.
      $description_build = $build;
    }

    $item = new \stdClass();
    $item->description = $description_build;
    $item->title = $support_ticket->label();
    $item->link = $support_ticket->link;
    // Provide a reference so that the render call in
    // template_preprocess_views_view_row_rss() can still access it.
    $item->elements = &$support_ticket->rss_elements;
    $item->stid = $support_ticket->id();

    return [
      '#theme' => $this->themeFunctions(),
      '#view' => $this->view,
      '#options' => $this->options,
      '#row' => $item,
    ];
  }

}
