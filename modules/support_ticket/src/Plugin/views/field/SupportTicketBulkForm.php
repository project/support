<?php

namespace Drupal\support_ticket\Plugin\views\field;

use Drupal\views\Plugin\views\field\BulkForm;
use Drupal\system\Plugin;

/**
 * Defines a support ticket operations bulk form element.
 *
 * @ViewsField("support_ticket_bulk_form")
 */
class SupportTicketBulkForm extends BulkForm {

  /**
   * {@inheritdoc}
   */
  protected function emptySelectedMessage() {
    return $this->t('No tickets selected.');
  }

}
