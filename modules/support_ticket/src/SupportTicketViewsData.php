<?php

namespace Drupal\support_ticket;

use Drupal\views\EntityViewsData;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides the views data for the support ticket entity type.
 */
class SupportTicketViewsData extends EntityViewsData {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['support_ticket_field_data']['table']['base']['weight'] = -10;
    $data['support_ticket_field_data']['table']['wizard_id'] = 'support_ticket';

    $data['support_ticket_field_data']['stid']['field']['argument'] = [
      'id' => 'support_ticket_stid',
      'name field' => 'title',
      'numeric' => TRUE,
      'validate type' => 'stid',
    ];

    $data['support_ticket_field_data']['title']['field']['default_formatter_settings'] = ['link_to_entity' => TRUE];

    $data['support_ticket_field_data']['title']['field']['link_to_support_ticket default'] = TRUE;

    $data['support_ticket_field_data']['type']['argument']['id'] = 'support_ticket_type';

    $data['support_ticket_field_data']['langcode']['help'] = $this->t('The language of the ticket or translation.');

    $data['support_ticket_field_data']['status']['filter']['label'] = $this->t('Published status');
    $data['support_ticket_field_data']['status']['filter']['type'] = 'yes-no';
    // Use status = 1 instead of status <> 0 in WHERE statement.
    $data['support_ticket_field_data']['status']['filter']['use_equal'] = TRUE;

    $data['support_ticket_field_data']['status_extra'] = [
      'title' => $this->t('Published status or admin user'),
      'help' => $this->t('Filters out unpublished support tickets if the current user cannot view it.'),
      'filter' => [
        'field' => 'status',
        'id' => 'support_ticket_status',
        'label' => $this->t('Published status or admin user'),
      ],
    ];

    $data['support_ticket_field_data']['locked']['filter']['label'] = $this->t('Locked support ticket status');

    $data['support_ticket']['path'] = [
      'field' => [
        'title' => $this->t('Path'),
        'help' => $this->t('The aliased path to this support ticket.'),
        'id' => 'support_ticket_path',
      ],
    ];

    $data['support_ticket']['support_ticket_bulk_form'] = [
      'title' => $this->t('Support ticket operations bulk form'),
      'help' => $this->t('Add a form element that lets you run operations on multiple support tickets.'),
      'field' => [
        'id' => 'support_ticket_bulk_form',
      ],
    ];

    // Bogus fields for aliasing purposes.
    $data['support_ticket_field_data']['created_fulldate'] = [
      'title' => $this->t('Created date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_fulldate',
      ],
    ];

    $data['support_ticket_field_data']['created_year_month'] = [
      'title' => $this->t('Created year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year_month',
      ],
    ];

    $data['support_ticket_field_data']['created_year'] = [
      'title' => $this->t('Created year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_year',
      ],
    ];

    $data['support_ticket_field_data']['created_month'] = [
      'title' => $this->t('Created month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_month',
      ],
    ];

    $data['support_ticket_field_data']['created_day'] = [
      'title' => $this->t('Created day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_day',
      ],
    ];

    $data['support_ticket_field_data']['created_week'] = [
      'title' => $this->t('Created week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'created',
        'id' => 'date_week',
      ],
    ];

    $data['support_ticket_field_data']['changed_fulldate'] = [
      'title' => $this->t('Updated date'),
      'help' => $this->t('Date in the form of CCYYMMDD.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_fulldate',
      ],
    ];

    $data['support_ticket_field_data']['changed_year_month'] = [
      'title' => $this->t('Updated year + month'),
      'help' => $this->t('Date in the form of YYYYMM.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year_month',
      ],
    ];

    $data['support_ticket_field_data']['changed_year'] = [
      'title' => $this->t('Updated year'),
      'help' => $this->t('Date in the form of YYYY.'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_year',
      ],
    ];

    $data['support_ticket_field_data']['changed_month'] = [
      'title' => $this->t('Updated month'),
      'help' => $this->t('Date in the form of MM (01 - 12).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_month',
      ],
    ];

    $data['support_ticket_field_data']['changed_day'] = [
      'title' => $this->t('Updated day'),
      'help' => $this->t('Date in the form of DD (01 - 31).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_day',
      ],
    ];

    $data['support_ticket_field_data']['changed_week'] = [
      'title' => $this->t('Updated week'),
      'help' => $this->t('Date in the form of WW (01 - 53).'),
      'argument' => [
        'field' => 'changed',
        'id' => 'date_week',
      ],
    ];

    $data['support_ticket_field_data']['uid']['help'] = $this->t('The user authoring the support ticket. If you need more fields than the uid add the support ticket: author relationship');
    $data['support_ticket_field_data']['uid']['filter']['id'] = 'user_name';
    $data['support_ticket_field_data']['uid']['relationship']['title'] = $this->t('Support ticket author');
    $data['support_ticket_field_data']['uid']['relationship']['help'] = $this->t('Relate support ticket to the user who created it.');
    $data['support_ticket_field_data']['uid']['relationship']['label'] = $this->t('author');

    $data['support_ticket']['support_ticket_listing_empty'] = [
      'title' => $this->t('Empty Support Ticket listing behavior'),
      'help' => $this->t('Provides a link to the support ticket add page.'),
      'area' => [
        'id' => 'support_ticket_listing_empty',
      ],
    ];

    $data['support_ticket_field_data']['uid_revision']['title'] = $this->t('User has a revision');
    $data['support_ticket_field_data']['uid_revision']['help'] = $this->t('All support tickets where a certain user has a revision');
    $data['support_ticket_field_data']['uid_revision']['real field'] = 'stid';
    $data['support_ticket_field_data']['uid_revision']['filter']['id'] = 'support_ticket_uid_revision';
    $data['support_ticket_field_data']['uid_revision']['argument']['id'] = 'support_ticket_uid_revision';

    $data['support_ticket_field_revision']['table']['wizard_id'] = 'support_ticket_revision';

    // Advertise this table as a possible base table.
    $data['support_ticket_field_revision']['table']['base']['help'] = $this->t('Support ticket revision is a history of changes to support tickets.');
    $data['support_ticket_field_revision']['table']['base']['defaults']['title'] = 'title';

    $data['support_ticket_field_revision']['stid']['argument'] = [
      'id' => 'support_ticket_stid',
      'numeric' => TRUE,
    ];
    $data['support_ticket_field_revision']['stid']['relationship']['id'] = 'standard';
    $data['support_ticket_field_revision']['stid']['relationship']['base'] = 'support_ticket_field_data';
    $data['support_ticket_field_revision']['stid']['relationship']['base field'] = 'stid';
    $data['support_ticket_field_revision']['stid']['relationship']['title'] = $this->t('Support ticket');
    $data['support_ticket_field_revision']['stid']['relationship']['label'] = $this->t('Get the actual support ticket from a support ticket revision.');

    $data['support_ticket_field_revision']['vid'] = [
      'argument' => [
        'id' => 'support_ticket_vid',
        'numeric' => TRUE,
      ],
      'relationship' => [
        'id' => 'standard',
        'base' => 'support_ticket_field_data',
        'base field' => 'vid',
        'title' => $this->t('Support ticket'),
        'label' => $this->t('Get the actual support ticket from a support ticket revision.'),
      ],
    ] + $data['support_ticket_field_revision']['vid'];

    $data['support_ticket_field_revision']['langcode']['help'] = $this->t('The language the original ticket is in.');

    $data['support_ticket_revision']['revision_uid']['help'] = $this->t('Relate a ticket revision to the user who created the revision.');
    $data['support_ticket_revision']['revision_uid']['relationship']['label'] = $this->t('revision user');

    $data['support_ticket_field_revision']['table']['wizard_id'] = 'support_ticket_field_revision';

    $data['support_ticket_field_revision']['table']['join']['support_ticket_field_data']['left_field'] = 'vid';
    $data['support_ticket_field_revision']['table']['join']['support_ticket_field_data']['field'] = 'vid';

    $data['support_ticket_field_revision']['status']['filter']['label'] = $this->t('Published');
    $data['support_ticket_field_revision']['status']['filter']['type'] = 'yes-no';
    $data['support_ticket_field_revision']['status']['filter']['use_equal'] = TRUE;

    $data['support_ticket_field_revision']['langcode']['help'] = $this->t('The language of the ticket or translation.');

    $data['support_ticket_field_revision']['link_to_revision'] = [
      'field' => [
        'title' => $this->t('Link to revision'),
        'help' => $this->t('Provide a simple link to the revision.'),
        'id' => 'support_ticket_revision_link',
        'click sortable' => FALSE,
      ],
    ];

    $data['support_ticket_field_revision']['revert_revision'] = [
      'field' => [
        'title' => $this->t('Link to revert revision'),
        'help' => $this->t('Provide a simple link to revert to the revision.'),
        'id' => 'support_ticket_revision_link_revert',
        'click sortable' => FALSE,
      ],
    ];

    $data['support_ticket_field_revision']['delete_revision'] = [
      'field' => [
        'title' => $this->t('Link to delete revision'),
        'help' => $this->t('Provide a simple link to delete the support ticket revision.'),
        'id' => 'support_ticket_revision_link_delete',
        'click sortable' => FALSE,
      ],
    ];

    // Add search table, fields, filters, etc., but only if a page using the
    // support_ticket_search plugin is enabled.
    if (\Drupal::moduleHandler()->moduleExists('search')) {
      $enabled = FALSE;
      $search_page_repository = \Drupal::service('search.search_page_repository');
      foreach ($search_page_repository->getActiveSearchpages() as $page) {
        if ($page->getPlugin()->getPluginId() == 'support_ticket_search') {
          $enabled = TRUE;
          break;
        }
      }

      if ($enabled) {
        $data['support_ticket_search_index']['table']['group'] = $this->t('Search');

        // Automatically join to the support_ticket_field_data table.
        // Use a Views table alias to allow other modules to use this table too,
        // if they use the search index.
        $data['support_ticket_search_index']['table']['join'] = [
          'support_ticket_field_data' => [
            'left_field' => 'stid',
            'field' => 'sid',
            'table' => 'search_index',
            'extra' => "support_ticket_search_index.type = 'support_ticket_search' AND support_ticket_search_index.langcode = support_ticket_field_data.langcode",
          ],
        ];

        $data['support_ticket_search_total']['table']['join'] = [
          'support_ticket_search_index' => [
            'left_field' => 'word',
            'field' => 'word',
          ],
        ];

        $data['search_ticket_dataset']['table']['join'] = [
          'support_ticket_field_data' => [
            'left_field' => 'sid',
            'left_table' => 'support_ticket_search_index',
            'field' => 'sid',
            'table' => 'search_dataset',
            'extra' => 'support_ticket_search_index.type = search_ticket_dataset.type AND support_ticket_search_index.langcode = search_ticket_dataset.langcode',
            'type' => 'INNER',
          ],
        ];

        $data['support_ticket_search_index']['score'] = [
          'title' => $this->t('Score'),
          'help' => $this->t('The score of the search item. This will not be used if the search filter is not also present.'),
          'field' => [
            'id' => 'search_score',
            'float' => TRUE,
            'no group by' => TRUE,
          ],
          'sort' => [
            'id' => 'search_score',
            'no group by' => TRUE,
          ],
        ];

        $data['support_ticket_search_index']['keys'] = [
          'title' => $this->t('Search Keywords'),
          'help' => $this->t('The keywords to search for.'),
          'filter' => [
            'id' => 'search_keywords',
            'no group by' => TRUE,
            'search_type' => 'support_ticket_search',
          ],
          'argument' => [
            'id' => 'search',
            'no group by' => TRUE,
            'search_type' => 'support_ticket_search',
          ],
        ];

      }
    }

    return $data;
  }

}
