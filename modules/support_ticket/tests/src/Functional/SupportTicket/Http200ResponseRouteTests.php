<?php

namespace Drupal\support_ticket\Tests\SupportTicket;


use Drupal\Tests\BrowserTestBase;

/**
 * Tests support tickets routes.
 *
 * @group support
 *
 * @codeCoverageIgnore
 */
class Http200ResponseRouteTests extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'diff',
    'comment',
    'field',
    'node',
    'options',
    'user',
    'views',
    'support',
    'support_ticket',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Testing to see if login works.
   */
  //public function testCorePageFunctionality() {
 //   $admin_user = $this->drupalCreateUser(['access content overview']);
  //  $this->drupalLogin($admin_user);
  //  $this->drupalGet('/admin/content');
  //  $this->assertSession()->statusCodeEquals(200);
 // }

  /**
   * Tests single support_ticket.add_page and support_ticket.add.
   *
   * Using the applications default created "ticket" as the variable
   * in the url's below to test the 200 response.
   * /support_ticket/add/.
   * /support_ticket/add/{support_ticket}.
   */
  public function testSupportTicketAddPageFunctionality() {
    $admin_user = $this->drupalCreateUser(
      [
        'administer add support tickets',
        'administer support ticket types'
      ]
    );
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types');
    $this->drupalGet('/support_ticket/add/ticket');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests support_ticket.multiple_delete_confirm.
   *
   * Using the applications default created "ticket" as the variable
   * in the url below to test the 200 response.
   * /support_ticket/{support_ticket}/delete.
   */
//  public function testSupportTicketMultipleDeleteConfirmFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/admin/support_ticket/ticket/delete');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Tests entity.support_ticket.preview.
   */
//  public function testSupportTicketPreviewFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/support_ticket/preview/{support_ticket_preview}/{view_mode_id}');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Tests entity.support_ticket.revision.
   */
//  public function testSupportTicketRevisionFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/support_ticket/{support_ticket}/revisions/{support_ticket_revision}/view');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Tests support_ticket.revision_delete_confirm.
   */
//  public function testSupportTicketRevisionDeleteConfirmFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/support_ticket/{support_ticket}/revisions/{support_ticket_revision}/delete');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Tests support_ticket.revision_revert_confirm.
   */
//  public function testSupportTicketRevisionRevertConfirmFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/support_ticket/{support_ticket}/revisions/{support_ticket_revision}/revert');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Tests entity.support_ticket.version_history.
   */
//  public function testSupportTicketVersionHistoryFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/support_ticket/{support_ticket}/revisions');
//    $this->assertSession()->statusCodeEquals(200);
//  }

  /**
   * Support ticket settings type routes.
   *
   * Tests support_ticket_settings.diff.
   */
//  public function testSupportTicketSettingsDiffFunctionality() {
//    $admin_user = $this->drupalCreateUser(['administer add support tickets']);
//    $this->drupalLogin($admin_user);
//    $this->drupalGet('/admin/config/content/diff/entities/support_ticket');
//    $this->assertSession()->statusCodeEquals(200);
//  }



}
