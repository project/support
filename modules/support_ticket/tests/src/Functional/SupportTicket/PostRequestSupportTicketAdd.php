<?php

namespace Drupal\support_ticket\Tests\SupportTicket_SupportTicketType_Integration;

use Drupal\Tests\BrowserTestBase;

/**
 * Testing support ticket type routes.
 *
 * @group support
 *
 * @codeCoverageIgnore
 */
class Integration extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'diff',
    'comment',
    'node',
    'options',
    'user',
    'views',
    'support',
    'support_ticket',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Testing Post support_ticket.type_add.
   */
  public function testSupportTicketPostTypeAddFunctionality():void {
    $admin_user = $this->drupalCreateUser(['administer support ticket types']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types/add');
    // Creates ticket types.
    // $this->drupalGet('/admin/structure/support_ticket/ticket-types/add');
    // $createPageType = $this->getSession()->getPage();
    // $createPageType->fillField('name', 'Ticket Test');
    //  $createPageType->fillField('type', 'ticket_test');
    // $createPageType->fillField('title_label', 'ticket');
    // $createPageType->fillField('description', 'ticket');
    // $createPageType->fillField('help', 'ticket');
    // $createPageType->findButton('Save support ticket type')->submit();
    $page = $this->getSession()->getPage();
    $page->fillField('name', 'Test ticket');
    $page->fillField('type', 'test_ticket');
    $page->fillField('title_label', 'Test ticket');
    $page->fillField('description', 'Test ticket');
    $page->fillField('help', 'Test ticket');
    $page->pressButton('Save support ticket type');
    $page->getContent();
    $this->assertSession()->statusCodeEquals(200);
  }

}
