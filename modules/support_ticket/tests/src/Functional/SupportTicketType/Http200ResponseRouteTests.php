<?php

namespace Drupal\support_ticket\Tests\SupportTicketType;

use Drupal\Tests\BrowserTestBase;

/**
 * Testing support ticket type routes.
 *
 * @group support
 *
 * @codeCoverageIgnore
 */
class Http200ResponseRouteTests extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'diff',
    'comment',
    'node',
    'options',
    'user',
    'views',
    'support',
    'support_ticket',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Testing drupal core path.
   */
  public function testCorePageFunctionality():void {
    $admin_user = $this->drupalCreateUser(['access content overview']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/content');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Testing support_ticket.type_add.
   */
  public function testSupportTicketTypeAddFunctionality():void {
    $admin_user = $this->drupalCreateUser(['administer support ticket types']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types/add');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Testing entity.support_ticket_type.collection.
   */
  public function testSupportTicketTypeCollectionFunctionality():void {
    $admin_user = $this->drupalCreateUser(['administer support ticket types']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Testing entity.support_ticket_type.delete_form.
   *
   * Using the applications default created "ticket" type as the variable
   * in the url below to test the 200 response.
   * /admin/structure/support_ticket/ticket-types/manage/{support_ticket_type}/delete.
   */
  public function testSupportTicketTypeDeleteFormFunctionality():void {
    $admin_user = $this->drupalCreateUser(['administer support ticket types']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types/manage/ticket/delete');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Testing entity.support_ticket_type.edit_form.
   *
   * Using the applications default created "ticket" type as the variable
   * in the url below to test the 200 response.
   * /admin/structure/support_ticket/ticket-types/manage/{support_ticket_type}.
   */
  public function testSupportTicketTypeEditFormFunctionality():void {
    $admin_user = $this->drupalCreateUser(['administer support ticket types']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('/admin/structure/support_ticket/ticket-types/manage/ticket');
    $this->assertSession()->statusCodeEquals(200);
  }

}
