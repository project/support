<?php

namespace Drupal\support_ticket\Tests;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Tests all the different buttons on the support_ticket form.
 *
 * @group support
 */
class SupportTicketFormButtonsTest extends SupportTicketTestBase {

  use AssertButtonsTrait;
  use StringTranslationTrait;

  /**
   * A normal logged in user.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $webUser;

  /**
   * A user with permission to bypass access content.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    // Create a user that has no access to change the state
    // of the support_ticket.
    $this->webUser = $this->drupalCreateUser(
      [
        'access support tickets',
        'create ticket ticket',
        'edit own ticket ticket',
      ]
    );
    // Create a user that has access to change the state of the support_ticket.
    // @todo 'administer support ticket types' is kinda silly as the admin
    // check for the page - look into the support_ticket/add permissions closer!
    $this->adminUser = $this->drupalCreateUser(
      [
        'access support tickets',
        'administer support tickets',
        'administer support ticket types',
        'view own unpublished support tickets',
      ]
    );
  }

  /**
   * Tests that the right buttons are displayed for saving support_tickets.
   */
  public function testSupportTicketFormButtons() {
    $support_ticket_storage = $this->container->get('entity_type.manager')->getStorage('support_ticket');
    // Login as administrative user.
    $this->drupalLogin($this->adminUser);

    // Verify the buttons on a support_ticket add form.
    $this->drupalGet('support_ticket/add/ticket');
    $this->assertButtons(
      [
        $this->t('Save and publish'),
        $this->t('Save as unpublished'),
      ]
    );

    // Save the support_ticket and assert it's published after clicking
    // 'Save and publish'.
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalPostForm('support_ticket/add/ticket', $edit, $this->t('Save and publish'));

    // Get the support_ticket.
    $support_ticket_1 = $support_ticket_storage->load(1);
    $this->assertTrue($support_ticket_1->isPublished(), 'Support ticket is published');

    // Verify the buttons on a support_ticket edit form.
    $this->drupalGet('support_ticket/' . $support_ticket_1->id() . '/edit');
    $this->assertButtons(
      [
        $this->t('Save and keep published'),
        $this->t('Save and unpublish'),
      ]
    );

    // Save the support_ticket and verify it's still published after clicking
    // 'Save and keep published'.
    $this->drupalPostForm(NULL, $edit, $this->t('Save and keep published'));
    $support_ticket_storage->resetCache([1]);
    $support_ticket_1 = $support_ticket_storage->load(1);
    $this->assertTrue($support_ticket_1->isPublished(), 'Support ticket is published');

    // Save the support_ticket and verify it's unpublished after clicking
    // 'Save and unpublish'.
    $this->drupalPostForm('support_ticket/' . $support_ticket_1->id() . '/edit', $edit, $this->t('Save and unpublish'));
    $support_ticket_storage->resetCache([1]);
    $support_ticket_1 = $support_ticket_storage->load(1);
    $this->assertFalse($support_ticket_1->isPublished(), 'Support ticket is unpublished');

    // Verify the buttons on an unpublished support_ticket edit screen.
    $this->drupalGet('support_ticket/' . $support_ticket_1->id() . '/edit');
    $this->assertButtons(
      [
        $this->t('Save and keep unpublished'),
        $this->t('Save and publish'),
      ]
    );

    // Create a support_ticket as a normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);

    // Verify the buttons for a normal user.
    $this->drupalGet('support_ticket/add/ticket');
    $this->assertButtons(
      [$this->t('Save')], FALSE);

    // Create the support_ticket.
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalPostForm('support_ticket/add/ticket', $edit, $this->t('Save'));
    $support_ticket_2 = $support_ticket_storage->load(2);
    $this->assertTrue($support_ticket_2->isPublished(), 'Support ticket is published');

    // Login as an administrator and unpublish the support_ticket that just
    // was created by the normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->adminUser);
    $this->drupalPostForm('support_ticket/' . $support_ticket_2->id() . '/edit', [], $this->t('Save and unpublish'));
    $support_ticket_storage->resetCache([2]);
    $support_ticket_2 = $support_ticket_storage->load(2);
    $this->assertFalse($support_ticket_2->isPublished(), 'Support ticket is unpublished');

    // Login again as the normal user, save the support_ticket and verify
    // it's still unpublished.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);
    $this->drupalPostForm('support_ticket/' . $support_ticket_2->id() . '/edit', [], $this->t('Save'));
    $support_ticket_storage->resetCache([2]);
    $support_ticket_2 = $support_ticket_storage->load(2);
    $this->assertFalse($support_ticket_2->isPublished(), 'Support ticket is still unpublished');
    $this->drupalLogout();

    // Set article content type default to unpublished. This will change the
    // the initial order of buttons and/or status of the support_ticket when
    // creating a support_ticket.
    $fields = \Drupal::entityManager()->getFieldDefinitions('support_ticket', 'ticket');
    $fields['status']->getConfig('ticket')
      ->setDefaultValue(FALSE)
      ->save();

    // Verify the buttons on a support_ticket add form for an administrator.
    $this->drupalLogin($this->adminUser);
    $this->drupalGet('support_ticket/add/ticket');
    $this->assertButtons(
      [
        $this->t('Save as unpublished'),
        $this->t('Save and publish'),
      ]
    );

    // Verify the support_ticket is unpublished by default for a normal user.
    $this->drupalLogout();
    $this->drupalLogin($this->webUser);
    $edit = ['title[0][value]' => $this->randomString()];
    $this->drupalPostForm('support_ticket/add/ticket', $edit, $this->t('Save'));
    $support_ticket_3 = $support_ticket_storage->load(3);
    $this->assertFalse($support_ticket_3->isPublished(), 'Support ticket is unpublished');

  }

}
