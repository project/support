<?php

namespace Drupal\Tests\support;

use Drupal\Tests\UnitTestCase;

/**
 * Support Unit Test class.
 *
 * @todo Add coversDefaultClass \Drupal\support\Support
 *
 * @group support
 */
class SupportUnitTest extends UnitTestCase {

  /**
   * Tests that unit tests work.
   */
  public function testGeneric() {
    // @todo Add real test code here.
    $this->assertEquals(TRUE, TRUE, 'Tautology is true.');
  }

}
