<?php

namespace Drupal\support\Tests;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the basic support integration functionality.
 *
 * @group support
 *
 * @codeCoverageIgnore
 */
class SupportInstallationTests extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'diff',
    'comment',
    'options',
    'user',
    'views',
    'support',
    'support_ticket',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Testing to see if login works.
   */
  public function testDrupalLoginFunctionality() {
    $admin_user = $this->drupalCreateUser(['access content']);
    $this->drupalLogin($admin_user);
  }

  /**
   * Tests front page work.
   */
  public function testSiteFrontPageFunctionality() {
    $this->drupalGet('<front>');
    $this->assertSession()->statusCodeEquals(200);
  }

}
